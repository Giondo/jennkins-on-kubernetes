resource "google_dns_record_set" "jenkins" {
  name = "jenkins.logicalislabs.ga."
  type = "A"
  ttl  = 300

  managed_zone = "logicalislabs"

  rrdatas = [var.JENKINSSERVICEIP]
}

