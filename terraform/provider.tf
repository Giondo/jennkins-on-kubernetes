provider "google" {
  project     = "equifax-demo-dev"
  region      = "northamerica-northeast1"
  zone        = "northamerica-northeast1-c"
}
