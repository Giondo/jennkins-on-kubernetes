terraform {
  backend "gcs" {
    bucket  = "equifax-terraform-states"
    prefix  = "terraform/state/dns-zone-logicalislabs-ga"
  }
}
