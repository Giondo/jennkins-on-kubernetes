#!/bin/bash
echo "Getting kube cluster credentials"
gcloud container clusters get-credentials kube-jenkins --zone northamerica-northeast1-c --project equifax-demo-dev
echo "Create tiller service account"
kubectl create serviceaccount tiller --namespace kube-system
echo "give tiller admin role to cluster"
kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin \
    --serviceaccount=kube-system:tiller
echo "heml init to create all tiller services inside kube cluster"
helm init --service-account=tiller
echo "helm repo update"
helm repo update
echo "Waiting 15 secs to deploy tiller inside kube cluster"
sleep 15
echo "deploy jenkins inside kube cluster"
#helm install -n cd stable/jenkins -f jenkins/values.yaml --version 1.2.2 --wait
helm install -n cd stable/jenkins -f jenkins/values.yaml --wait
echo "Getting Service IP to create dns record"
SERVICE_IP=$(kubectl get svc --namespace default cd-jenkins --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")
cat <<EOF > terraform/vars.tf
variable "JENKINSSERVICEIP" {
	default = "$SERVICE_IP"
}
EOF
echo "Running Terraform to create dns record"
cd terraform
terraform init 
terraform validate 
terraform plan -detailed-exitcode -out=plan-name.out 
terraform apply plan-name.out 

echo "Create Production Namespace on Kubernetes"
kubectl create ns production
echo "Create canary Namespace on Kubernetes"
kubectl create ns canary
echo "Configure jenkins service account on Kubernetes"
kubectl create clusterrolebinding jenkins-deploy --clusterrole=cluster-admin --serviceaccount=default:cd-jenkins
